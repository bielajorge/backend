FROM openjdk:8-jdk-alpine
LABEL maintainer="desenvolvimento@mboss.com.br"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "-Djava.security.egd=file:/dev/./urandow", "/app.jar"]

# use "mvn clean package dockerfile:build" para fazer o build
# para iniciar o docker, use "docker run -p 8080:8080 -t caos/caos"