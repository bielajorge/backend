package com.mboss.caos.user.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.caos.response.Response;
import com.mboss.caos.security.entity.User;
import com.mboss.caos.security.enums.Role;
import com.mboss.caos.security.service.UserServiceImpl;

@RestController
@RequestMapping("/user")
public class UserController {

	//private static final Logger log = LoggerFactory.getLogger(AuthenticationController.class);

	@Autowired
	private UserServiceImpl userService;

	@Autowired
	private BCryptPasswordEncoder cryp;
	
	@RequestMapping(path = "/test", method = RequestMethod.GET)
	public ResponseEntity<Response<String>> teste() {
		Response<String> response = new Response<>();
		User user = new User();
		user.setPassword("1425613");
		user.setUsername("raphael");
		user.setRole(Role.ROLE_ADMIN);
		Optional<User> oUser = userService.save(user);
		if (!oUser.isPresent())
			response.getErrors().add("Não foi possível cadastrar o usuário!");
		else
			response.setData("OK");
		return ResponseEntity.ok(response);
	}

	@RequestMapping(path = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response<String>> addUser(@RequestBody User user) {
		Response<String> response = new Response<>();
		if (user.getPassword().isEmpty() || user.getUsername().isEmpty()) {
			response.getErrors().add("Preencha os campos usuário e senha!");
			return ResponseEntity.ok(response);
		}

		Optional<User> oUser;
		oUser = userService.findByUsername(user.getUsername());
		if (oUser.isPresent()) {
			response.getErrors().add("Usuário já existente.");
			return ResponseEntity.ok(response);
		}

		user.setPassword(cryp.encode(user.getPassword()));
		user.setRole(Role.ROLE_USER);
		oUser = userService.save(user);
		if (!oUser.isPresent())
			response.getErrors().add("Não foi possível cadastrar o usuário!");
		else
			response.setData("OK");
		
		return ResponseEntity.ok(response);
	}

}
