package com.mboss.caos.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mboss.caos.security.JwtUserFactory;
import com.mboss.caos.security.entity.User;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> funcionario = userService.findByUsername(username);
		if (funcionario.isPresent())
			return JwtUserFactory.create(funcionario.get());
		
		throw new UsernameNotFoundException("Usuário não encontrado.");
	}

}
