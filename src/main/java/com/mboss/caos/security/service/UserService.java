package com.mboss.caos.security.service;

import java.util.Optional;

import com.mboss.caos.security.entity.User;

public interface UserService {

	/**
	 * Busca e retorna um usuário dado um email.
	 * 
	 * @param email
	 * @return Optional<Usuario>
	 */
	Optional<User> findByUsername(String username);

}
