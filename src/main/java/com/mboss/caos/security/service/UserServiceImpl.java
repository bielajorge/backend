package com.mboss.caos.security.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mboss.caos.security.entity.User;
import com.mboss.caos.security.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public Optional<User> findByUsername(String username) {
		return Optional.ofNullable(this.userRepository.findByUsername(username));
	}
	
	public Optional<User> save(User user) {
		user = userRepository.save(user);
		if (user.getId() > 0)
			return Optional.ofNullable(user);			
		return Optional.empty();
	}
	
	
}
