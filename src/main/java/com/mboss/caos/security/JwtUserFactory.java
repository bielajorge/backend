package com.mboss.caos.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.mboss.caos.security.entity.User;
import com.mboss.caos.security.enums.Role;

public class JwtUserFactory {

	private JwtUserFactory() {
	}

	/** Converte e gera um JwtUser com base nos dados de um funcionário.
	 * 
	 * @param funcionario
	 * @return JwtUser */
	public static JwtUser create(User usuario) {
		return new JwtUser(usuario.getId(), usuario.getUsername(), usuario.getPassword(),
				mapToGrantedAuthorities(usuario.getRole()));
	}

	/** Converte o perfil do usuário para o formato utilizado pelo Spring Security.
	 * 
	 * @param perfilEnum
	 * @return List<GrantedAuthority> */
	private static List<GrantedAuthority> mapToGrantedAuthorities(Role perfilEnum) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(perfilEnum.toString()));
		return authorities;
	}

}
