package com.mboss.caos.security.enums;

public enum Role {
	ROLE_ADMIN, ROLE_USER;
}
