package com.mboss.caos.user.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.nio.charset.Charset;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.mboss.caos.AppTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest extends AppTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mock;

	@Autowired
	private UserController controller;

	private String userNameVazio = "{ \"username\": \"\", \"password\": \"kzs8\" }";
	private String userPasswordVazio = "{ \"username\": \"Administrador\", \"password\": \"\" }";
	private String user = "{ \"username\": \"rezzieri\", \"password\": \"1425613\" }";

	@Before
	public void setUp() {
		this.mock = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void retornaATrueSeUsuarioNomeVazio() throws Exception {
		MvcResult result = this.mock
				.perform(MockMvcRequestBuilders.post("/user/add").content(userNameVazio).contentType(contentType))
				.andExpect(status().isOk()).andReturn();

		JSONObject objJson = new JSONObject(result.getResponse().getContentAsString());
		JSONArray errors = objJson.getJSONArray("errors");
		assertEquals(true, errors.length() > 0);
	}

	@Test
	public void retornaA1TrueSeUsuarioSenhaVazio() throws Exception {
		MvcResult result = this.mock
				.perform(MockMvcRequestBuilders.post("/user/add").content(userPasswordVazio).contentType(contentType))
				.andExpect(status().isOk()).andReturn();

		JSONObject objJson = new JSONObject(result.getResponse().getContentAsString());
		JSONArray errors = objJson.getJSONArray("errors");
		assertEquals(true, errors.length() > 0);
	}

	@Test
	public void retornaBTrueSeAdicionouUsuario() throws Exception {
		MvcResult result = this.mock
				.perform(MockMvcRequestBuilders.post("/user/add").content(user).contentType(contentType))
				.andExpect(status().isOk()).andReturn();

		JSONObject objJson = new JSONObject(result.getResponse().getContentAsString());
		String token = objJson.getString("data");
		assertEquals(true, token.equals("OK"));
	}

	@Test
	public void retornaB1FalseUsuarioJaExiste() throws Exception {
		MvcResult result = this.mock
				.perform(MockMvcRequestBuilders.post("/user/add").content(user).contentType(contentType))
				.andExpect(status().isOk()).andReturn();

		JSONObject objJson = new JSONObject(result.getResponse().getContentAsString());
		JSONArray errors = objJson.getJSONArray("errors");
		assertEquals(false, !(errors.length() > 0));
	}
	
}
